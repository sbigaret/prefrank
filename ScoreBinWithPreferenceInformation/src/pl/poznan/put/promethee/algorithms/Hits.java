package pl.poznan.put.promethee.algorithms;

import java.util.List;
import java.util.Map;

public class Hits extends PageRank {

	public Hits(Matrix M) {
		super(M.mul(M.transpose()));
		// TODO Auto-generated constructor stub
	}

	public Hits(Map<String, Map<String, Double>> matrix, List<String> alternatives_ids) {
		this(new Matrix(matrix,alternatives_ids));
		// TODO Auto-generated constructor stub
	}
	public Hits(Matrix M,boolean transopse) {
		super(transopse ?  M.transpose().mul(M) : M.mul(M.transpose()));
		// TODO Auto-generated constructor stub
	}

	public Hits(Map<String, Map<String, Double>> matrix, List<String> alternatives_ids, boolean transopse) {
		this(new Matrix(matrix,alternatives_ids),transopse);
		// TODO Auto-generated constructor stub
	}

}
