import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	FlowsXMCDATest_v2.class, 
	FlowsXMCDATest_v3.class, 
	FlowsXMCDARunTest_v2_ok.class,
	FlowsXMCDARunTest_v2_warning.class,
	FlowsXMCDARunTest_v2_error.class,
	FlowsXMCDARunTest_v3_ok.class})
public class AllTests {

}
