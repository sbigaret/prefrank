package pl.poznan.put.promethee.algorithms;

import java.util.List;
import java.util.Map;

public class TrustSalsa extends TrustRank {

	public TrustSalsa(Map<String, Map<String, Double>> matrix, List<String> alternatives_ids, Matrix D, double q) {
		this(new Matrix(matrix, alternatives_ids), D, q);
	}

	public TrustSalsa(Matrix M, Matrix D, double q) {
		super(M.normailzeColumns().mul(M.normailzeRows().transpose()), D, q);
	}
	
	public TrustSalsa(Map<String, Map<String, Double>> matrix, List<String> alternatives_ids, Matrix D, double q, boolean transpose) {
		this(new Matrix(matrix, alternatives_ids), D, q,transpose);
	}

	public TrustSalsa(Matrix M, Matrix D, double q,boolean transpose) {
		super(transpose ? M.normailzeRows().transpose().mul(M.normailzeColumns()) :M.normailzeColumns().mul(M.normailzeRows().transpose()), D, q);
	}

}
