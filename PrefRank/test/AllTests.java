import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ FlowsXMCDATest_v2.class, FlowsXMCDATest_v3.class })
public class AllTests {

}
