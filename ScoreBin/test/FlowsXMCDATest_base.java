import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.xmcda.ProgramExecutionResult;

import pl.poznan.put.promethee.flows.ResultContainer;
import pl.poznan.put.promethee.xmcda.FlowsXMCDA;
import pl.poznan.put.promethee.xmcda.Utils;
import pl.poznan.put.promethee.xmcda.InputsHandler.AlgorithmType;

public class FlowsXMCDATest_base {
	protected String inputDirectory;
	protected String outputDirectory;
	protected Utils.XMCDA_VERSION version;
	protected ResultContainer xmcdaResults;
	private String[] alternatives_ids;
	
    public FlowsXMCDATest_base(String inputDirectory, String outputDirectory,Utils.XMCDA_VERSION version ) {
		this.inputDirectory = inputDirectory;
		this.outputDirectory = outputDirectory;
		this.version = version;

    }
    
    protected boolean Test(ResultType resultType) {
    	System.out.println(inputDirectory);
		
		final File prgExecResultsFile = new File(outputDirectory, "messages.xml");

		final ProgramExecutionResult executionResult = new ProgramExecutionResult();
		
		xmcdaResults = FlowsXMCDA.run(prgExecResultsFile, executionResult, inputDirectory, outputDirectory, version);

		try {
			Utils.writeProgramExecutionResults(prgExecResultsFile, executionResult, version);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			return false;			
		}
		switch (resultType) {
		case OK:		
			return executionResult.isOk();

		case ERROR:
			return executionResult.isError();
		case WARNING:
			return executionResult.isWarning();
		}
		return false;
		
    }
    
    
    protected static Collection<Object[]> getData(String test_folder,Utils.XMCDA_VERSION version){
    	List<String> folders_in  =  ListFolders(test_folder,"in");
    	List<String> folders_out =  ListFolders(test_folder,"out");
    	
    	Object[][] combined = new Object[folders_in.size()][];
    	for(int i =0;i<folders_in.size();i++)
    	{
    		combined[i] = new Object[3];
    		combined[i][0] = folders_in.get(i);
    		combined[i][1] = folders_out.get(i);
    		combined[i][2] = version;
    	}

        return Arrays.asList(combined);
    }
    protected static List<String> ListFolders(String folder,String endsWith){
		try (Stream<Path> walk = Files.walk(Paths.get(folder))) {

			List<String> result = walk.filter(Files::isDirectory)
					.map(x -> x.toString()).filter(f -> f.endsWith(endsWith)).collect(Collectors.toList());

			return result;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
    
    
    public  void EqualFlowTest() {		

		HashMap<String,HashMap<String,Double>> values = null;
				
		Map<String, Double> positiveFlows = null;
		Map<String, Double> negativeFlows = null;
		Map<String, Double> totalFlows = null;
		
		if (this.xmcdaResults.getInputs().algorithmType == AlgorithmType.TYPE_1) {
			
			values = GetDataTestFile(inputDirectory,"type_1");
			positiveFlows = values.get("PR");
			negativeFlows = values.get("PRT");
			totalFlows = values.get("total flow PR");
		}
		else if (this.xmcdaResults.getInputs().algorithmType == AlgorithmType.TYPE_2) {
			values = GetDataTestFile(inputDirectory,"type_2");
			positiveFlows = values.get("HITS Hub");
			negativeFlows = values.get("HITS Auth");
			totalFlows = values.get("total flow HITS");
			
		}
		else if (this.xmcdaResults.getInputs().algorithmType == AlgorithmType.TYPE_3) {
			values = GetDataTestFile(inputDirectory,"type_3");
			positiveFlows = values.get("Salsa Hub");
			negativeFlows = values.get("Salsa Auth");
			totalFlows = values.get("total flow Salsa");
		}

		

		
		IsEqualFlow("positiveFlows",positiveFlows,this.xmcdaResults.getPositive_flows());
		IsEqualFlow("negativeFlows",negativeFlows,this.xmcdaResults.getNegative_flows());
		IsEqualFlow("totalFlows",totalFlows,this.xmcdaResults.getTotal_flows());
	}
    
    protected HashMap<String,HashMap<String,Double>> GetDataTestFile(String inputDirectory,String algorithm) {
    	String file = inputDirectory.split("\\\\")[2].replace("pair_", "").replace("crisp_", "").replace("matrix_", "").replace(algorithm+"_in", "test_data.csv");
    	return ReadCSV(file);
    }
    	
    protected void IsEqualFlow(String message, Map<String, Double> f1 ,Map<String, Double> f2) {
    	assertEquals(f1.size(),f2.size());
    	for(int i =0;i<f1.size();i++) {
    		String key = alternatives_ids[i+1];
    		assertEquals(message+" alternative: "+key,f1.get(key), f2.get(key),0.001);
    		
    	}
    }
    
    protected HashMap<String,HashMap<String,Double>> ReadCSV(String file) {
		HashMap<String,HashMap<String,Double>> values = null;
		BufferedReader csvReader;
		try {
			csvReader = new BufferedReader(new FileReader(file));
			String row;
			alternatives_ids = null;
			values = new HashMap<String,HashMap<String,Double>>();
			boolean headers = true;
			while ((row = csvReader.readLine()) != null) {
			    String[] data = row.split(",");
			    if(headers) {
			    	headers = false;
			    	alternatives_ids = data;
			    }
			    else
			    {
			    	HashMap<String,Double> metric = new HashMap<String,Double>();
			    	for(int i =1;i<alternatives_ids.length;i++) {
			    		double value = Double.parseDouble(data[i]);
			    		metric.put(alternatives_ids[i], value);			    		
			    	}
			    	values.put(data[0], metric);
			    }

			}
			csvReader.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return values;
	}
}
