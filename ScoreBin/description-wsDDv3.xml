<?xml version='1.0' encoding='utf-8'?>
<program_description>
	<program provider="PUT" name="Outranking-ScoreBin_scores" version="1.0.0" displayName="Outranking-ScoreBin_scores"/>
	<documentation>
		<description>Module for calculation ScoreBin scores. The preferences can be given as a complete pairwise comparison matrix where the value determines the degree of preference 0.0 or 1.0. The second type of preference input is to specify pairs of alternatives for which there is a preference.</description>
		<contact><![CDATA[Krzysztof Martyn <krzysztof.martyn@wp.pl>]]></contact>
		<url>https://bitbucket.org/Krzysztof_Martyn/prefrank</url>
	</documentation>
	<parameters>
		<input id="input1" name="alternatives" displayName="alternatives" isoptional="0">
			<documentation>
				<description>Alternatives to consider.</description>
			</documentation>
			<xmcda tag="alternatives"/>
		</input>
		<input id="input2" name="preferences" displayName="preferences" isoptional="0">
			<documentation>
				<description>Aggregated preferences binary matrix or pairs for which the outrank relationship occurs.</description>
			</documentation>
			<xmcda tag="alternativesMatrix"/>
		</input>
		<input id="input3" name="parameters" displayName="parameters" isoptional="0">
			<documentation>
				<description>First parameter specifies if preference are given by matrix or pairs. Second parameter specifies the algorithm to calculate ranking. There are three algorithms to choose from: PageRank, HITS and Salsa.
				</description>
			</documentation>
			<xmcda tag="programParameters"><![CDATA[
        
		<programParameters>
			<parameter id="input_type" name="input_type">
				<values>
					<value>
						<label>%1</label>
					</value>
				</values>
			</parameter>	
			<parameter id="algorithm_type" name="algorithm_type">
				<values>
					<value>
						<label>%2</label>
					</value>
				</values>
			</parameter>
			<parameter id="number_of_iteration" name="number_of_iteration">
				<values>
					<value>
						<integer>%3</integer>
					</value>
				</values>
			</parameter>
			<parameter id="check_convergence" name="check_convergence">
				<values>
					<value>
						<boolean>%4</boolean>
					</value>
				</values>
			</parameter>
			<parameter id="early_stopping" name="early_stopping">
				<values>
					<value>
						<boolean>%5</boolean>
					</value>
				</values>
			</parameter>
		</programParameters>
        
      ]]></xmcda>
			<gui status="preferGUI">
				<entry id="%1" type="enum" displayName="input type">
					<items>
						<item id="item0">
							<description>Preferences given by whole matrix 1-0 valued.</description>
							<value>matrix</value>
						</item>
						<item id="item1">
							<description>Preferences given by pairs for whom the outranking relationship occurs (crisp).</description>
							<value>pair</value>
						</item>
					</items>
					<defaultValue>item0</defaultValue>
				</entry>
				<entry id="%2" type="enum" displayName="algorithm type">
					<items>
						<item id="item0">
							<description>ScoreBin I</description>
							<value>scorebin_1</value>
						</item>
						<item id="item1">
							<description>ScoreBin II</description>
							<value>scorebin_2</value>
						</item>
						<item id="item2">
							<description>ScoreBin III</description>
							<value>scorebin_3</value>
						</item>
					</items>
					<defaultValue>item0</defaultValue>
				</entry>
				<entry id="%3" type="int" displayName="number of iteration">
					<documentation>
						<description>Number of iteration</description>
					</documentation>
					<constraint>
						<description>The value should be greater than 1</description>
						<code><![CDATA[ %3 > 1]]></code>
					</constraint>
					<defaultValue>1000</defaultValue>
				</entry>
				<entry id="%4" type="boolean" displayName="Run averaging if not converge?">
					<documentation>
						<description>Turning on the version with averaging the score value, if after "number of iteration" iteration the score does not converge, that is: the maximum score difference between the last two iterations is greater than 0.001.</description>
					</documentation>
					<defaultValue>false</defaultValue>
				</entry>
				<entry id="%5" type="boolean" displayName="Earlier stop if converge?">
					<documentation>
						<description>Earlier stop if the maximum difference in score between two consecutive iterations is less than 0.00001</description>
					</documentation>
					<defaultValue>true</defaultValue>
				</entry>
			</gui>
		</input>
		<output id="output1" name="positive_flows" displayName="positive flows">
			<documentation>
				<description>Positive outranking flows.</description>
			</documentation>
			<xmcda tag="alternativesValues"/>
		</output>
		<output id="output2" name="negative_flows" displayName="negative flows">
			<documentation>
				<description>Negative outranking flows.</description>
			</documentation>
			<xmcda tag="alternativesValues"/>
		</output>
		<output id="output3" name="total_flows" displayName="total flows">
			<documentation>
				<description>Final flows computed from the given data.</description>
			</documentation>
			<xmcda tag="alternativesValues"/>
		</output>
		<output id="output4" name="ranking" displayName="ranking">
			<documentation>
				<description>ScoreBin scores computed from the given data.</description>
			</documentation>
			<xmcda tag="alternativesMatrix"/>
		</output>
		<output id="output5" name="messages" displayName="messages">
			<documentation>
				<description>Messages or errors generated by this module.</description>
			</documentation>
			<xmcda tag="programExecutionResult"/>
		</output>
	</parameters>
</program_description>
