package pl.poznan.put.promethee.algorithms;

import java.util.List;
import java.util.Map;

public class TrustHits extends TrustRank {
	public TrustHits(Matrix M, Matrix D,double q) {
		super(M.mul(M.transpose()),D,q);
		// TODO Auto-generated constructor stub
	}

	public TrustHits(Map<String, Map<String, Double>> matrix, List<String> alternatives_ids, Matrix D, double q) {
		this(new Matrix(matrix, alternatives_ids),D,q);
		// TODO Auto-generated constructor stub
	}
	
	public TrustHits(Map<String, Map<String, Double>> matrix, List<String> alternatives_ids, Matrix D,double q, boolean transopse) {
		this(new Matrix(matrix, alternatives_ids),D,q,transopse);
		// TODO Auto-generated constructor stub
	}
	
	public TrustHits(Matrix M, Matrix D,double q,boolean transopse) {
		super(transopse ?  M.transpose().mul(M) : M.mul(M.transpose()),D,q);
		// TODO Auto-generated constructor stub
	}


}
