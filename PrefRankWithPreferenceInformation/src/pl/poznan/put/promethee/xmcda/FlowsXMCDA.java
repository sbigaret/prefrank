/**
 *
 */
package pl.poznan.put.promethee.xmcda;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import org.xmcda.ProgramExecutionResult;
import org.xmcda.XMCDA;

import javafx.util.Pair;
import pl.poznan.put.promethee.flows.Flows;
import pl.poznan.put.promethee.flows.Ranker;
import pl.poznan.put.promethee.flows.ResultContainer;
import pl.poznan.put.promethee.xmcda.Utils.InvalidCommandLineException;

/**
 * 
 */
public class FlowsXMCDA {
	/**
	 * 
	 * 
	 * @param args
	 * @throws InvalidCommandLineException 
	 */
	public static void main(String[] args) throws InvalidCommandLineException {
		final Utils.XMCDA_VERSION version = readVersion(args);
		final Utils.Arguments params = readParams(args);

		final String inputDirectory = params.inputDirectory;
		final String outputDirectory = params.outputDirectory;
		
		final File prgExecResultsFile = new File(outputDirectory, "messages.xml");

		final ProgramExecutionResult executionResult = new ProgramExecutionResult();
		System.out.println(inputDirectory);
		run(prgExecResultsFile, executionResult, inputDirectory, outputDirectory, version);
		
		System.out.println("OK: "+executionResult.isOk());
		exitProgram(executionResult, prgExecResultsFile, version);
	}
	
	public static ResultContainer run(File prgExecResultsFile, ProgramExecutionResult executionResult, String inputDirectory, String outputDirectory, Utils.XMCDA_VERSION version) {
		Map<String, InputFile> files = initFiles();		

		final XMCDA xmcda = InputFileLoader.loadFiles(files, inputDirectory, executionResult, prgExecResultsFile,
				version);
		if (!ErrorChecker.checkErrors(executionResult, xmcda))
			return null;

		final InputsHandler.Inputs inputs = InputsHandler.checkAndExtractInputs(xmcda, executionResult);
		if (!ErrorChecker.checkErrors(executionResult, inputs))
			return null;

		final Map<String, Map<String, Double>> flows = calcFlows(inputs, executionResult);
		if (!ErrorChecker.checkErrors(executionResult, flows))
			return null ;
		
		final Map<Pair<String, String>,Integer> ranking = calcRanking(inputs,flows, executionResult);
		if (!ErrorChecker.checkErrors(executionResult, flows))
			return null ;
		
		final Map<String, XMCDA> xmcdaResults = OutputsHandler.convert(flows,ranking,inputs.alternatives_ids,executionResult);

		OutputFileWriter.writeResultFiles(xmcdaResults, executionResult, outputDirectory, version);
		
		return new ResultContainer(flows, ranking, inputs);
	}
	
	private static Utils.Arguments readParams(String[] args) {
		Utils.Arguments params = null;
		ArrayList<String> argsList = new ArrayList<String>(Arrays.asList(args));
		argsList.remove("--v2");
		argsList.remove("--v3");
		try {
			params = Utils.parseCmdLineArguments((String[]) argsList.toArray(new String[] {}));
		} catch (InvalidCommandLineException e) {
			System.err.println("Missing mandatory options. Required: [--v2|--v3] -i input_dir -o output_dir");
			System.exit(-1);
		}
		return params;
	}

	private static Utils.XMCDA_VERSION readVersion(String[] args) {
		Utils.XMCDA_VERSION version = Utils.XMCDA_VERSION.v2;
		;
		final ArrayList<String> argsList = new ArrayList<String>(Arrays.asList(args));
		if (argsList.remove("--v2")) {
			version = Utils.XMCDA_VERSION.v2;
		} else if (argsList.remove("--v3")) {
			version = Utils.XMCDA_VERSION.v3;
		} else {
			System.err.println("Missing mandatory option --v2 or --v3");
			System.exit(-1);
		}
		return version;
	}

	private static Map<String, InputFile> initFiles() {
		Map<String, InputFile> files = new LinkedHashMap<>();
		files.put("methodParameters",
				new InputFile("methodParameters", "programParameters", "parameters.xml", true));
		files.put("alternatives", new InputFile("alternatives", "alternatives", "alternatives.xml", true));		
		files.put("preferenceInformation", new InputFile("alternativesValues", "alternativesValues", "preference_information.xml", false));		
		
		files.put("preferences",
				new InputFile("alternativesComparisons", "alternativesMatrix", "preferences.xml", true));
		return files;
	}

	private static void exitProgram(ProgramExecutionResult executionResult, File prgExecResultsFile,
			Utils.XMCDA_VERSION version) {
		Utils.writeProgramExecutionResultsAndExit(prgExecResultsFile, executionResult, version);
	}

	private static Map<String, Map<String, Double>> calcFlows(InputsHandler.Inputs inputs,
			ProgramExecutionResult executionResult) {
		FlowsResult results = null;
		try {
			results = Flows.calcFlows(inputs);
			if (!results.isConvergentStrenght()) {
				executionResult.addWarning("The calculated strenght does not converge.");
			}
			if (!results.isConvergentWeakness()) {
				executionResult.addWarning("The calculated weakness does not converge.");
			}

		} catch (Throwable t) {
			executionResult.addError(Utils.getMessage("The calculation could not be performed, reason: ", t));
			return null;
		}
		return results.getFlows();
	
	}
	
	private static Map<Pair<String, String>,Integer>  calcRanking(InputsHandler.Inputs inputs, Map<String, Map<String, Double>> flows,
			ProgramExecutionResult executionResult) {
		Map<Pair<String, String>,Integer> results = null;
		try {
			results = Ranker.Ranking(inputs.alternatives_ids,flows);
		} catch (Throwable t) {
			executionResult.addError(Utils.getMessage("The calculation could not be performed, reason: ", t));
			return results;
		}
		return results;
	}
}
