package pl.poznan.put.promethee.flows;

import java.util.Map;

import javafx.util.Pair;
import pl.poznan.put.promethee.xmcda.InputsHandler.Inputs;

public class ResultContainer {
	private Map<String, Double> positive_flows;
	private Map<String, Double> negative_flows;
	private Map<String, Double> total_flows;
	private Map<Pair<String, String>,Integer> ranking;
	private Inputs inputs;
	
	public ResultContainer(Map<String, Map<String, Double>> flows, Map<Pair<String, String>,Integer> ranking, Inputs inputs ) {
		this.positive_flows = flows.get("positive_flows");
		this.negative_flows = flows.get("negative_flows");
		this.total_flows = flows.get("total_flows");
		this.ranking = ranking;
		this.inputs = inputs;
	}


	public Inputs getInputs() {
		return inputs;
	}

	public Map<String, Double> getPositive_flows() {
		return positive_flows;
	}

	public Map<String, Double> getNegative_flows() {
		return negative_flows;
	}

	public Map<String, Double> getTotal_flows() {
		return total_flows;
	}

	public Map<Pair<String, String>, Integer> getRanking() {
		return ranking;
	}
}
