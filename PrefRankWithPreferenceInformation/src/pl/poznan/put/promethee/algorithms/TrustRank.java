package pl.poznan.put.promethee.algorithms;

import java.util.List;
import java.util.Map;

public class TrustRank extends PageRank{
	protected double q;
	protected Matrix D;
	
	public TrustRank(Map<String, Map<String, Double>> matrix, List<String> alternatives_ids, Matrix D, double q2) {
		super(matrix, alternatives_ids);
		this.D = D;
		this.Vec = D.Copy();
		this.q = q2;
	}

	public TrustRank(Matrix M,Matrix D,double q) {
		super(M);
		this.D = D;
		this.Vec = D.Copy();
		this.q = q;
	}
	
	public TrustRank(Map<String, Map<String, Double>> matrix, List<String> alternatives_ids, Matrix D, double q2, boolean transpose) {
		this(new Matrix(matrix, alternatives_ids),D,q2,transpose);
	}

	public TrustRank(Matrix M,Matrix D,double q,boolean transpose) {
		super(M,transpose);
		this.D = D;
		this.Vec = D.Copy();
		this.q = q;
	}
	
	@Override
	protected void UpdateVec() {
		this.Vec = this.D.mul(q).add(this.M.mul(this.Vec).mul(1-q));
	}
}
