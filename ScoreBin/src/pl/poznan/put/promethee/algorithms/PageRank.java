package pl.poznan.put.promethee.algorithms;

import java.util.List;
import java.util.Map;

public class PageRank extends BasicAlgorithm{


	public PageRank(Map<String, Map<String, Double>> matrix, List<String> alternatives_ids) {
		this(new Matrix(matrix, alternatives_ids));
		// TODO Auto-generated constructor stub
	}

	public PageRank(Matrix M) {
		super(M);
		// TODO Auto-generated constructor stub
	}
	public PageRank(Map<String, Map<String, Double>> matrix, List<String> alternatives_ids, boolean transpose) {
		this(new Matrix(matrix, alternatives_ids),transpose);
		// TODO Auto-generated constructor stub
	}

	public PageRank(Matrix M,boolean transpose) {
		super(M,transpose);
		// TODO Auto-generated constructor stub
	}
	protected void UpdateVec() {
		this.Vec = this.M.mul(this.Vec);
	}
	
	public void run(int nr_iter, boolean checkConvergence, boolean earlyStopping) {
		for(int iter=0;iter<nr_iter;iter++)
		{		
			this.VecPrev = this.Vec.Copy();
			UpdateVec();
			this.Vec._normailzeColumns();	
			if(this.Vec.ElemSum()<0.001) {
				this.Vec = this.VecPrev;
				break;
			}
			else if(earlyStopping && this.Vec.subtract(this.VecPrev).CheckAbsLowerThan(0.00001)) {
				break;
			}
				
		}
		if(this.Vec.subtract(this.VecPrev).CheckAbsLowerThan(0.001)) {
			this.isConvergent = true;
		}
		else{
			if(checkConvergence)
			{		
				for(int iter=0;iter<nr_iter;iter++)
				{
					this.VecPrev = this.Vec.Copy();
					UpdateVec();
					this.Vec = (this.Vec.add(this.VecPrev))._mul(0.5);
					this.Vec._normailzeColumns();
					if(this.Vec.ElemSum()<0.001) {
						this.Vec = this.VecPrev;
						break;
					}
					else if(earlyStopping && this.Vec.subtract(this.VecPrev).CheckAbsLowerThan(0.00001)) {
						break;
					}
				}
			}
			if(this.Vec.subtract(this.VecPrev).CheckAbsLowerThan(0.001)) {
				this.isConvergent = true;
			}
			else{
				this.isConvergent = false;
			}
		}
	}
}
