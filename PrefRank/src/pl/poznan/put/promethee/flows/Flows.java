package pl.poznan.put.promethee.flows;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import pl.poznan.put.promethee.algorithms.BasicAlgorithm;
import pl.poznan.put.promethee.algorithms.Hits;
import pl.poznan.put.promethee.algorithms.Matrix;
import pl.poznan.put.promethee.algorithms.PageRank;
import pl.poznan.put.promethee.algorithms.Salsa;
import pl.poznan.put.promethee.xmcda.FlowsResult;
import pl.poznan.put.promethee.xmcda.InputsHandler.AlgorithmType;
import pl.poznan.put.promethee.xmcda.InputsHandler.Inputs;

public class Flows {
	public static FlowsResult calcFlows(Inputs inputs) {
		Map<String, Map<String, Double>> flows = new LinkedHashMap<String, Map<String, Double>>();

		BasicAlgorithm algorithmStrenght = null;
		BasicAlgorithm algorithmWeakness = null;
		
		if(inputs.algorithmType == AlgorithmType.TYPE_1) 
		{
			algorithmStrenght = new PageRank(inputs.preferences, inputs.alternatives_ids);
			algorithmWeakness = new PageRank(inputs.preferences, inputs.alternatives_ids,true);
			
		}
		else if(inputs.algorithmType ==AlgorithmType.TYPE_2) 
		{
			algorithmStrenght = new Hits(inputs.preferences, inputs.alternatives_ids);
			algorithmWeakness = new Hits(inputs.preferences, inputs.alternatives_ids,true);
		}
		else 
		{
			algorithmStrenght = new Salsa(inputs.preferences, inputs.alternatives_ids);
			algorithmWeakness = new Salsa(inputs.preferences, inputs.alternatives_ids,true);
		}
		
		algorithmStrenght.run(inputs.numberOfIteration, inputs.checkConvergence, inputs.earlyStopping);
		algorithmWeakness.run(inputs.numberOfIteration, inputs.checkConvergence, inputs.earlyStopping);
		Matrix total = algorithmStrenght.Get().subtract(algorithmWeakness.Get());
		
		Map<String, Double> positiveFlows = algorithmStrenght.GetResult();
		Map<String, Double> negativeFlows = algorithmWeakness.GetResult();
		Map<String, Double> totalFlows = total.GetResult();
		
		
		flows.put("positive_flows", sortMapByKey(positiveFlows));
		flows.put("negative_flows", sortMapByKey(negativeFlows));
		flows.put("total_flows", sortMapByKey(totalFlows));
		

		return new FlowsResult(flows, 
				algorithmStrenght.isConvergent(),
				algorithmWeakness.isConvergent());
	}


	private static Map<String, Double> sortMapByKey(Map<String, Double> map) {
		Map<String, Double> sortedMap = map.entrySet().stream().sorted(Entry.comparingByKey())
				.collect(Collectors.toMap(Entry::getKey, Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		return sortedMap;
	}
}
