import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import pl.poznan.put.promethee.xmcda.Utils;

@RunWith(Parameterized.class)
public class FlowsXMCDARunTest_v3_ok extends FlowsXMCDATest_base{
    @Parameters
    public static Collection<Object[]> data() {
    	
    	String test_folder = "tests/run_test_v3_ok";
    	Utils.XMCDA_VERSION version = Utils.XMCDA_VERSION.v3;

        return getData(test_folder,version);
    }


    
    public FlowsXMCDARunTest_v3_ok(String inputDirectory, String outputDirectory,Utils.XMCDA_VERSION version ) {
    	super(inputDirectory,outputDirectory,version);

    }

    @Test
	public  void test() {		
		Assert.assertTrue(Test(ResultType.OK));
	}
    
    
}