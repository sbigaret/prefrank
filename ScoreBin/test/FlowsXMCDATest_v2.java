import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import pl.poznan.put.promethee.xmcda.Utils;

@RunWith(Parameterized.class)
public class FlowsXMCDATest_v2 extends FlowsXMCDATest_base{
    @Parameters
    public static Collection<Object[]> data() {
    	
    	String test_folder = "tests/v2";
    	Utils.XMCDA_VERSION version = Utils.XMCDA_VERSION.v2;

        return getData(test_folder,version);
    }


    
    public FlowsXMCDATest_v2(String inputDirectory, String outputDirectory,Utils.XMCDA_VERSION version ) {
    	super(inputDirectory,outputDirectory,version);

    }

    @Test
	public  void test() {		
		Assert.assertTrue(Test(ResultType.OK));
		EqualFlowTest();
	}
    
    
}