#! /bin/bash

source common_envvars.sh

if [ $# != 1 ]; then
  echo "Usage: ${0} [--v2|--v3]" >&2
  exit 1
elif [ ${1} != "--v2" -a ${1} != "--v3" ]; then
  echo "Usage: ${0} [--v2|--v3]" >&2
  exit 1
fi

version=${1#*v}

mkdir -p tests_tmp

failed=0
while read -d '' -r IN; do
    REFERENCE_OUT="${IN%_in}_out"
    OUT=$(mktemp --tmpdir=. -d tests_tmp/out.XXX)
    echo "${IN}"
    ${CMD} "--v${version}" -i "${IN}" -o "${OUT}"
    diff -x README -ruBw "${REFERENCE_OUT}" "${OUT}"
    ret_diff=$?
    if [ $ret_diff -ne 0 ]; then
        echo -e "${RED}✘${OFF} ${IN}"
        ((failed++))
    else
        echo -e "${GREEN}√${OFF} ${IN}"
        rm -r ${OUT}
    fi
    #read -s -n 1 -p "Press any key to continue..." <&7; echo ""
done < <( find tests/*v${version}* -maxdepth 1 -type d -name "*_in" -print0 ) # 7<&1
[ $failed -ne 0 ] && echo -e "${RED}TESTS FAILED${OFF} -- # of failures: $failed"
exit $failed
