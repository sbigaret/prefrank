package pl.poznan.put.promethee.algorithms;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class BasicAlgorithm {
	protected Matrix M;
	protected Matrix Vec;
	protected Matrix VecPrev;
	protected boolean isConvergent;
	
	public BasicAlgorithm(Map<String, Map<String, Double>> matrix, List<String> alternatives_ids)
	{
		this(new Matrix(matrix, alternatives_ids));
	}

	public BasicAlgorithm(Matrix M,boolean transpose) {
		this( transpose ? M.transpose() :M);
	}
	public BasicAlgorithm(Matrix M) {
		this.M = M;
		Vec = new Matrix(M.nrows,M.index);
		Vec.init(1.);
		VecPrev = Vec.Copy();	
	}

	public void T() {
		this.M = this.M.transpose();
	}
	public Matrix Get() {
		return this.Vec;
	}
	
	public abstract void run(int nr_iter, boolean checkConvergence, boolean earlyStopping) ;
	
	public LinkedHashMap<String, Double> GetResult(){	
		return this.Vec.GetResult();
		
	}
	public boolean isConvergent() {
		return this.isConvergent;
	}
	protected abstract void UpdateVec();
}
