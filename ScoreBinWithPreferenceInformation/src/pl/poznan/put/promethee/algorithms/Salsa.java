package pl.poznan.put.promethee.algorithms;

import java.util.List;
import java.util.Map;

public class Salsa extends PageRank {

	public Salsa(Map<String, Map<String, Double>> matrix, List<String> alternatives_ids) {
		this(new Matrix(matrix, alternatives_ids));
		// TODO Auto-generated constructor stub
	}

	public Salsa(Matrix M) {			
		super(M.normailzeColumns().mul(M.normailzeRows().transpose()));

	}
	
	public Salsa(Map<String, Map<String, Double>> matrix, List<String> alternatives_ids, boolean transpose) {
		this(new Matrix(matrix, alternatives_ids),transpose);
		// TODO Auto-generated constructor stub
	}

	public Salsa(Matrix M,boolean transpose) {			
		super(transpose ? M.normailzeRows().transpose().mul(M.normailzeColumns()) :M.normailzeColumns().mul(M.normailzeRows().transpose()));
		
		// TODO Auto-generated constructor stub
	}

}
