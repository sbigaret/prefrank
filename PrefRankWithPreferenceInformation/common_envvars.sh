# Adapt the following lines
# NB: javafx.util.Pair is required: either install javafx or use Oracle Java 8
JAVA_HOME="${JAVA_HOME:-/usr/local/jdk1.8.0_181}"
# These 2 instruct the XMCDA Java lib to use a specific version when writing
export XMCDAv2_VERSION=2.2.3
export XMCDAv3_VERSION=3.1.1

# -- You normally do not need to change anything beyond this point --
JARS="./lib/xmcda-0.9.jar:target/PrefRankWithPreferenceInformation.jar"

JAVA="${JAVA_HOME}/bin/java"
export JAVA_HOME
CMD="${JAVA} -cp ${JARS} pl.poznan.put.promethee.xmcda.FlowsXMCDA"

RED="\e[0;91m"
GREEN="\e[0;92m"
BOLD="\e[1m"
OFF="\e[0m"
