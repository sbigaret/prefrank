package pl.poznan.put.promethee.xmcda;

import org.xmcda.Alternative;
import org.xmcda.AlternativesMatrix;
import org.xmcda.ProgramExecutionResult;
import org.xmcda.ProgramParameter;
import org.xmcda.QualifiedValues;
import org.xmcda.value.Type;
import org.xmcda.XMCDA;
import org.xmcda.utils.Coord;
import org.xmcda.utils.Matrix;
import org.xmcda.utils.ValueConverters.ConversionException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 
 */
public class InputsHandler {
	public enum InputType {
		MATRIX("matrix"), PAIRS("pair");

		private String label;

		private InputType(String paramLabel) {
			label = paramLabel;
		}

		/**
		 * Return the label for this InputType Parameter
		 *
		 * @return the parameter's label
		 */
		public final String getLabel() {
			return label;
		}

		/**
		 * Returns the parameter's label
		 *
		 * @return the parameter's label
		 */
		@Override
		public String toString() {
			return label;
		}

		/**
		 * Returns the {@link InputType} with the specified label. It
		 * behaves like {@link #valueOf(String)} with the exception
		 *
		 * @param parameterLabel
		 *            the label of the constant to return
		 * @return the enum constant with the specified label
		 * @throws IllegalArgumentException
		 *             if there is no InputType with this label
		 * @throws NullPointerException
		 *             if parameterLabel is null
		 */
		public static InputType fromString(String parameterLabel) {
			if (parameterLabel == null)
				throw new NullPointerException("parameterLabel is null");
			for (InputType op : InputType.values()) {
				if (op.toString().equals(parameterLabel))
					return op;
			}
			throw new IllegalArgumentException("Enum InputType with label " + parameterLabel + " not found");
		}
	}
	
	public enum AlgorithmType {
		TYPE_1("scorebin_1"), TYPE_2("scorebin_2"),TYPE_3("scorebin_3");

		private String label;

		private AlgorithmType(String paramLabel) {
			label = paramLabel;
		}

		/**
		 * Return the label for this AlgorithmType Parameter
		 *
		 * @return the parameter's label
		 */
		public final String getLabel() {
			return label;
		}

		/**
		 * Returns the parameter's label
		 *
		 * @return the parameter's label
		 */
		@Override
		public String toString() {
			return label;
		}

		/**
		 * Returns the {@link AlgorithmType} with the specified label. It
		 * behaves like {@link #valueOf(String)} with the exception
		 *
		 * @param parameterLabel
		 *            the label of the constant to return
		 * @return the enum constant with the specified label
		 * @throws IllegalArgumentException
		 *             if there is no AlgorithmType with this label
		 * @throws NullPointerException
		 *             if parameterLabel is null
		 */
		public static AlgorithmType fromString(String parameterLabel) {
			if (parameterLabel == null)
				throw new NullPointerException("parameterLabel is null");
			for (AlgorithmType op : AlgorithmType.values()) {
				if (op.toString().equals(parameterLabel))
					return op;
			}
			throw new IllegalArgumentException("Enum AlgorithmType with label " + parameterLabel + " not found");
		}
	}
	
	public static class Inputs {
		public InputType inputType;
		public AlgorithmType algorithmType;
		public List<String> alternatives_ids;
		public List<String> profiles_ids;
		public Map<String, Map<String, Double>> preferences;
		public boolean checkConvergence;
		public int numberOfIteration;
		public boolean earlyStopping;
	}

	/**
	 *
	 * @param xmcda
	 * @param xmcda_exec_results
	 * @return
	 */
	static public Inputs checkAndExtractInputs(XMCDA xmcda, ProgramExecutionResult xmcda_exec_results) {
		Inputs inputs = new Inputs();
		checkParameters(inputs, xmcda, xmcda_exec_results);
		checkAlternatives(inputs, xmcda, xmcda_exec_results);	
		if (xmcda_exec_results.isError())
			return null;
		
		extractAlternatives(inputs, xmcda, xmcda_exec_results);
		checkPreferences(inputs, xmcda, xmcda_exec_results);
		checkNumberOfPreferences(inputs, xmcda, xmcda_exec_results);
		if (xmcda_exec_results.isError())
			return null;
		
		extractPreferences(inputs, xmcda, xmcda_exec_results);	
		checkExtractedPreferences(inputs, xmcda_exec_results);
		if (xmcda_exec_results.isError())
			return null;
		
		return inputs;

	}

	/**
	 * @param xmcda
	 * @param errors
	 * @return Inputs
	 */


	private static void checkParameters(Inputs inputs, XMCDA xmcda, ProgramExecutionResult errors) {
		InputType inputType = null;
		AlgorithmType algorithmType = null;

		if (xmcda.programParametersList.size() > 1) {
			errors.addError("Only one list of parameters is expected");
			return;
		}
		if (xmcda.programParametersList.size() == 0) {
			errors.addError("List of parameters not found");
			return;
		}
		if (xmcda.programParametersList.get(0).size() != 5) {
			errors.addError("Exactly five parameter are expected");
			return;
		}

		//final ProgramParameter<?> prgParam = xmcda.programParametersList.get(0).get(0);

		for(ProgramParameter<?> prgParam : xmcda.programParametersList.get(0))
		{
			if ("input_type".equals(prgParam.name())) {
				
				if (prgParam.getValues() == null || (prgParam.getValues() != null && prgParam.getValues().size() != 1)) {
					errors.addError("input_type parameter must have a single (label) value only");
					return;
				}
				try {
					final String parameterValue = (String) prgParam.getValues().get(0).getValue();
					inputType = InputType.fromString((String) parameterValue);
				} catch (Throwable throwable) {
					StringBuffer valid_values = new StringBuffer();
					for (InputType op : InputType.values()) {
						valid_values.append(op.getLabel()).append(", ");
					}
					String err = "Invalid value for input_type parameter, it must be a label, ";
					err += "possible values are: " + valid_values.substring(0, valid_values.length() - 2);
					errors.addError(err);
					inputType = null;
				}
				inputs.inputType = inputType;
				
			}
			else if ("algorithm_type".equals(prgParam.name())) {
				
				if (prgParam.getValues() == null || (prgParam.getValues() != null && prgParam.getValues().size() != 1)) {
					errors.addError("algorithm_type parameter must have a single (label) value only");
					return;
				}
				try {
					final String parameterValue = (String) prgParam.getValues().get(0).getValue();
					algorithmType = AlgorithmType.fromString((String) parameterValue);
				} catch (Throwable throwable) {
					StringBuffer valid_values = new StringBuffer();
					for (AlgorithmType op : AlgorithmType.values()) {
						valid_values.append(op.getLabel()).append(", ");
					}
					String err = "Invalid value for algorithm_type parameter, it must be a label, ";
					err += "possible values are: " + valid_values.substring(0, valid_values.length() - 2);
					errors.addError(err);
					algorithmType = null;
				}
				inputs.algorithmType = algorithmType;
				
			}
			else if ("check_convergence".equals(prgParam.name())) {
				
				if (prgParam.getValues() == null || (prgParam.getValues() != null && prgParam.getValues().size() != 1)) {
					errors.addError("check_convergence parameter must have a single (boolean) value only");
					return;
				}
				try {
					inputs.checkConvergence = (Boolean) prgParam.getValues().get(0).getValue();

				} catch (Throwable throwable) {					
					String err = "Invalid value for check_convergence parameter, it must be a boolean type, ";
					err += "possible values are: true, false";
					errors.addError(err);
				}
				
			}
			else if ("number_of_iteration".equals(prgParam.name())) {
				
				if (prgParam.getValues() == null || (prgParam.getValues() != null && prgParam.getValues().size() != 1)) {
					errors.addError("check_convergence parameter must have a single (integer) value only");
					return;
				}
				try {
					inputs.numberOfIteration = (Integer) prgParam.getValues().get(0).getValue();
					if(inputs.numberOfIteration<1) {
						errors.addError("number_of_iteration must be positive greater than 1.");
					}
				} catch (Throwable throwable) {
					
					String err = "Invalid value for algorithm_type parameter, it must be a integer.";

					errors.addError(err);
				}
			}
			else if ("early_stopping".equals(prgParam.name())) {
				
				if (prgParam.getValues() == null || (prgParam.getValues() != null && prgParam.getValues().size() != 1)) {
					errors.addError("early_stopping parameter must have a single (boolean) value only");
					return;
				}
				try {
					inputs.earlyStopping = (Boolean) prgParam.getValues().get(0).getValue();

				} catch (Throwable throwable) {					
					String err = "Invalid value for early_stopping parameter, it must be a boolean type, ";
					err += "possible values are: true, false";
					errors.addError(err);
				}
				
			}
			else {

				errors.addError(String.format("Invalid parameter '%s'", prgParam.id()));
				return;
				
			}
		}
		
		

		
	}

	private static void checkAlternatives(Inputs inputs, XMCDA xmcda, ProgramExecutionResult errors) {
		if (xmcda.alternatives.size() == 0) {
			errors.addError("Alternatives not found");
			return;
		}
		if (xmcda.alternatives.getActiveAlternatives().size() == 0) {
			errors.addError("Active alternatives not found");
			return;
		}
	}

	private static void checkPreferences(Inputs inputs, XMCDA xmcda, ProgramExecutionResult errors) {
		if (xmcda.alternativesMatricesList.isEmpty()) {
			errors.addError("List of preferences is empty");
			return;
		}
		if (xmcda.alternativesMatricesList.size() == 0) {
			errors.addError("List of preferences has not been supplied");
			return;
		}
		if (xmcda.alternativesMatricesList.size() != 1) {
			errors.addError("Exactly one list of preferences is expected");
			return;
		}
		if (xmcda.alternativesMatricesList.get(0).isEmpty()) {
			errors.addError("List of preferences is empty");
			return;
		}
		if (! xmcda.alternativesMatricesList.get(0).isSimple()) {
			errors.addError("Preferences can only contain single values for a pair of alternatives.");
			return;
		}
		if (! xmcda.alternativesMatricesList.get(0).isHomogeneous()) {
			errors.addError("Preferences can only contain values of the same type.");
			return;
		}
		
		if (inputs.inputType == InputType.MATRIX)
		{
			try {
				xmcda.alternativesMatricesList.get(0).checkConversion(Double.class);
			} catch (ConversionException e) {
				errors.addError("List of preferences contains illegal preference values, only real values (0.0 and 1.0) are allowed.");
			}
		}
	}

	/**
	 *
	 * @param inputs
	 * @param xmcda
	 * @param xmcda_execution_results
	 * @return
	 */


	private static void extractAlternatives(Inputs inputs, XMCDA xmcda, ProgramExecutionResult errors) {
		List<String> alternativesIds = xmcda.alternatives.getActiveAlternatives().stream()
				.filter(a -> "alternatives.xml".equals(a.getMarker())).map(Alternative::id)
				.collect(Collectors.toList());
		if (alternativesIds.isEmpty())
			errors.addError("The alternatives list can not be empty.");
		inputs.alternatives_ids = alternativesIds;
	}

	private static void checkNumberOfPreferences(Inputs inputs, XMCDA xmcda, ProgramExecutionResult errors) {
		AlternativesMatrix<Double> preferences = (AlternativesMatrix<Double>) xmcda.alternativesMatricesList.get(0);
		HashMap<String, List<String>> matrix = new HashMap<>();
		for (String alternative : inputs.alternatives_ids) {
			matrix.put(alternative, new ArrayList<String>());
		}
		
		for (Coord<Alternative, Alternative> coord : preferences.keySet()) {
			String x = coord.x.id();
			String y = coord.y.id();
			if(matrix.get(x).contains(y)) {
				errors.addError("Preference of alternatives "+x+" "+y+" appearing multiple times.");						
				return;
			}
			matrix.get(x).add(y);
		}
		boolean full_matrix = true;
		for (String alternative : inputs.alternatives_ids) {
			for (String alternative2 : inputs.alternatives_ids) {
				if (!matrix.get(alternative).contains(alternative2)) {	
					if(inputs.inputType == InputType.MATRIX) {
						errors.addError(
								"In list of preferences doesn't exist preference to alternatives: " + alternative + " and "+alternative2 
								+". The input type is set to matrix. Check that the preferences are given as a matrix, not as a list of pairs.");
						return;
					}
					full_matrix = false;
				}
			}
		}
		if(inputs.inputType==InputType.PAIRS) {
			if(full_matrix) {		
				errors.addWarning(
						"Preferences contain the complete preferences matrix." 
						+" The input type is set to pair. Check that the preferences are given as a pairs, not as a matrix.");
				return;
			}
			/*
			for (Coord<Alternative, Alternative> coord : preferences.keySet()) {
				if( preferences.get(coord).get(0).isNumeric()){
					errors.addWarning(
							"The input type is set to pair, but preference of alternatives "+coord.x.id()+", "+coord.y.id()+" has a value that will be ignored.");
				}
			}
			*/
		}
	}
	private static void extractPreferences(Inputs inputs, XMCDA xmcda, ProgramExecutionResult errors) {
		@SuppressWarnings("unchecked")
		AlternativesMatrix<Double> preferences = (AlternativesMatrix<Double>) xmcda.alternativesMatricesList.get(0);
		inputs.preferences = new LinkedHashMap<String, Map<String, Double>>();
		
		for (Coord<Alternative, Alternative> coord : preferences.keySet()) {
			String x = coord.x.id();
			String y = coord.y.id();
			
			inputs.preferences.putIfAbsent(x, new HashMap<>());
			Double value;
			
			if (inputs.inputType==InputType.PAIRS && Type.isNA(preferences.getCellsType()[0])){
				value = 1.;
			}
			else {
				value = preferences.get(coord).get(0).getValue().doubleValue();
				
			}
			inputs.preferences.get(x).put(y, value);
		}		
	}

	private static void checkExtractedPreferences(Inputs inputs, ProgramExecutionResult errors) {
		boolean empty_matrix = true;
		if ((inputs.alternatives_ids != null) && (inputs.preferences != null)) {
			for (String alternative : inputs.alternatives_ids) {
				if (!inputs.preferences.containsKey(alternative) & inputs.inputType == InputType.PAIRS) {
					inputs.preferences.put(alternative, new HashMap<String, Double>());
				}
				
				if (inputs.preferences.containsKey(alternative)) {
					for (String alternative2 : inputs.alternatives_ids) {
						if (inputs.preferences.get(alternative).containsKey(alternative2)) {							
							Double value = inputs.preferences.get(alternative).get(alternative2);
							if( !((value ==0.0) || (value == 1.0))){
								errors.addError(
										"In list of preferences preference isn't binary");
								return;
							}
							empty_matrix=false;
						
						}
						else if(inputs.inputType==InputType.MATRIX) {
							errors.addError(
									"In list of preferences doesn't exist preference to alternatives: " + alternative + " and "+alternative2);
							return;
						}
						else {
							inputs.preferences.get(alternative).put(alternative2, 0.0);
						}
					}
				}else if(inputs.inputType==InputType.MATRIX){
					errors.addError("In list of preferences doesn't exist alternative: " + alternative);
					return;
				}
			}
		}
		if (empty_matrix) {
			errors.addError("In list of preferences doesn't exist any preference");
			return;
		}
			
		
		

	}
}
